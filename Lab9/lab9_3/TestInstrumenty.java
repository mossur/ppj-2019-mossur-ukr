import pl.imiujd.mossur.*;

import java.util.*;
import java.time.LocalDate;
import java.util.ArrayList;
public class TestInstrumenty
{
    public static void main(String[] args)
    {
        ArrayList<Instrument> orkiestra = new ArrayList<Instrument>();
        orkiestra.add(new Flet("Filero", LocalDate.of(1990, 10, 20)));
        orkiestra.add(new Fortepian("Filero", LocalDate.of(1990, 10, 20)));
        orkiestra.add(new Skrzypce("Filero", LocalDate.of(1990, 10, 20)));
        orkiestra.add(new Skrzypce("Filero", LocalDate.of(1990, 10, 20)));
        orkiestra.add(new Flet("Filero", LocalDate.of(1990, 10, 20)));
        for (Instrument p : orkiestra) {
            System.out.println(p+" "+p.dzwiek());
        }
        System.out.println("0==4"+orkiestra.get(0).equals(orkiestra.get(4)));
    }
}

