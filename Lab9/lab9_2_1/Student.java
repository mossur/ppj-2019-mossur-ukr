class Student extends Osoba
{
    public Student(String nazwisko, String imie, String kierunek,LocalDate dataUrodzenia, double sredniaOcen, boolean plec)
    {
        super(nazwisko,imie,dataUrodzenia,plec);
        this.kierunek = kierunek;
        this.sredniaOcen=sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek + "; sr.ocen: "+sredniaOcen;
    }
    public double getOcen()
    {
        return sredniaOcen;
    }
    public void setOcen(double sredniaOcen)
    {
        this.sredniaOcen=sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;
}

