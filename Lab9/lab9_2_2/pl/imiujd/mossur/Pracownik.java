package pl.imiujd.mossur;
import java.time.LocalDate;
public class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, String imie, double pobory, LocalDate dataZatrudnienia,LocalDate dataUrodzenia, boolean plec)
    {
        super(nazwisko,imie,dataUrodzenia,plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }
    public LocalDate getDataZatrudnienia()
    {
        return dataZatrudnienia;
    }
    private double pobory;
    private LocalDate dataZatrudnienia;
}
