import java.util.ArrayList;

public class lab7_5
{
    public static void reverse(ArrayList<Integer> a)
    {
        ArrayList<Integer> b = new ArrayList<Integer>();
        for(int i = a.size()-1; i>=0; i--)
        {
        b.add(a.get(i));
        }
        a = b;
        System.out.println(a);
    }

	public static void main(String[] args) 
	{
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1); a.add(3); a.add(5); a.add(23); a.add(100);
        reverse(a);
    }
}

