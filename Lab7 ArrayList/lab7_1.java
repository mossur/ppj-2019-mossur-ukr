import java.util.ArrayList;

public class lab7_1
{
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        for(int i = 0; i<b.size(); i++)
        {
            a.add(b.get(i));
        }
        return a;
    }

	public static void main(String[] args) 
	{
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1); a.add(3); a.add(5);
        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(2); b.add(6); b.add(8);
        System.out.println(append(a,b));

    }
}

