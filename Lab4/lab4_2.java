import java.util.Scanner;
import java.io.File;

public class lab4_2
{
    public static int countChar(String str, char c)
	{	
		int temp=0;
		for (int i=0; i<str.length(); i++)
		{
		if (str.charAt(i)==c) temp++;
		}
		return temp;
	}
    

	public static void main(String[] args)  throws Exception 
	{
        File file = new File(args[0]);
        Scanner sc = new Scanner(file);
        char znak = args[1].charAt(0);
        int S = 0;
        while(sc.hasNext())
        {
            String a = sc.next();
            S+=countChar(a, znak);
        }
        System.out.println(S);
	}
}
