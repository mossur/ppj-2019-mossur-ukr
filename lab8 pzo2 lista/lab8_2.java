public class lab8_2
{
	public static void main(String[] args) 
	{
        Adres A = new Adres("Karpenka_Karogo", 9, "Lutsk", 43024);
        A.pokaz();
        Adres B = new Adres("Gulaka_Artemowskogo", 3, "Lutsk", 304, 43000);
        B.pokaz();
        System.out.println("A przed B: " + A.przed(B));
        System.out.println("B przed A: " + B.przed(A));
    }
}
class Adres
{
    private String ulica;
        private int numer_domu;
        private int numer_mieszkania;
        private String miasto;
        private int kod_pocztowy;
        Adres(String ulica, int numer_domu, String miasto, int numer_mieszkania, int kod_pocztowy)
        {
            this.ulica = ulica;
            this.numer_domu = numer_domu;
            this.numer_mieszkania=numer_mieszkania;
            this.miasto = miasto;
            this.kod_pocztowy = kod_pocztowy;
        }
        Adres(String ulica, int numer_domu, String miasto, int kod_pocztowy)
        {
            this.ulica = ulica;
            this.numer_domu = numer_domu;
           // this.numer_mieszkania=-1;
            this.miasto = miasto;
            this.kod_pocztowy = kod_pocztowy;
        }
        public void pokaz()
        {
            System.out.println("Kod Pocztowy: "+kod_pocztowy + " Miasto: " + miasto);
            System.out.println("Numer Domu: "+numer_domu + " Numer Mieszkania: " + numer_mieszkania);
        }
        public boolean przed(Adres A)
        {
            if(this.kod_pocztowy>A.kod_pocztowy) return false;
            else return true;
        }
}

