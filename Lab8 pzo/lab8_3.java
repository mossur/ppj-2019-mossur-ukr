public class lab8_3
{
	public static void main(String[] args) 
	{
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        saver1.printC();
        saver2.printC();
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        saver1.printC();
        saver2.printC();
        
    }
}
class RachunekBankowy
{
    static double rocznaStopaProcentowa;
    private double saldo;
    RachunekBankowy(double saldo)
    {
        this.saldo=saldo;
    }
    public void obliczMiesieczneOdsetki()
    {
        saldo += saldo * rocznaStopaProcentowa/12;
    }
    public static void setRocznaStopaProcentowa(double a)
    {
        rocznaStopaProcentowa=a;
    }
    public void printC()
    {
        System.out.println(this.saldo);
    }
}

