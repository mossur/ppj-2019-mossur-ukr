import java.util.*;
public class Lab12_1
{
    public static void main(String[] args)
    {
        LinkedList<String> b = new LinkedList<>();
        b.add("Tatiana");
        b.add("Sasha");
        b.add("Diana");
        b.add("Nadia");
        b.add("Bodia");
        b.add("Denis");
        redukuj(b, 2);
        System.out.println(b);
    }
    public static void redukuj(LinkedList<String> pracownicy, int n)
    {
        Iterator<String> bIter = pracownicy.listIterator();
        while (bIter.hasNext()) 
        {
            boolean tst = true;
            for(int i = 0; i<n; i++)
            {
                if (bIter.hasNext()) bIter.next();
                else tst = false;
            }
            if(tst) bIter.remove();
        }
    }
}
