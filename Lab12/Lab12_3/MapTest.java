// Na podstawie książki Cay Horstmann, Gary Cornell.
// "Java (TM). Podstawy." Wydanie VIII

import java.util.*;

public class MapTest
{
    public static void main(String[] args)
    {
        int id = 1;
        Map<Student, String> personel = new TreeMap<Student, String>();
        Map<Integer, Student> ids = new TreeMap<Integer, Student>();
        Student a = new Student("Mossur","Tetiana",id);
        personel.put(a, "bdb+");
        ids.put(id,a);
        // wydruk wszystkich pozycji
        // usunięcie wartości
        //personel.remove("567-24-2546");

        // podmienienie pozycji
        //personel.put("456-62-5527", new Pracownik("Francesca Miller"));

        // wyszukanie wartości
        //System.out.println(personel.get("157-62-7935"));

        // iteracja przez wszystkie pozycje
        Scanner in = new Scanner(System.in);
        while(true)
        {
            String cmd = in.nextLine();
            if(cmd.equals("show")) show(personel);
            if(cmd.equals("add"))
            {
                 id++;
                 add(personel,id,ids);
            }
            if(cmd.equals("rem")) rem(ids,personel);
        }

    }
    static void show(Map<Student, String> Mapa)
    {
        for (Map.Entry<Student, String> wpis : Mapa.entrySet()) 
        {
            Student key = wpis.getKey();
            String value = wpis.getValue();
            System.out.println(key + ": " + value);
        }
    }
    static void add(Map<Student, String> Mapa,int id, Map<Integer, Student> ids)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Nazwisko? ");
        String naz = in.next();
        System.out.println("Imie? ");
        String im = in.next();
        System.out.println("Ocena? ");
        String oc = in.next();
        Student a = new Student(naz,im,id);
        Mapa.put(a, oc);
        ids.put(id, a);
        System.out.println("Dodano");
    }
    static void rem(Map<Integer, Student> ids, Map<Student, String> personel)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Id? ");
        int id = in.nextInt();
        personel.remove(ids.get(id));
        System.out.println("Removed");
    }
}


class Student implements Comparable<Student>
{
    public Student(String nazwisko, String imie, int id)
    {
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.id = id;
    }
    public String toString()
    {
        return "nazwisko=" + nazwisko
               + ",imie=" + imie
               + ",id=" + id;
    }
    public int compareTo(Student other)
    {
        int result = nazwisko.compareTo(other.nazwisko);
        if(result!=0) return result;
        result = imie.compareTo(other.imie);
        if(result!=0) return result;
        result = id - other.id;
        if(result!=0) return result/Math.abs(result);
        return result;
    }
    private String nazwisko;
    private String imie;
    private int id;
}


