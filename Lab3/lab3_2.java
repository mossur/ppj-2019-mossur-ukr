import java.util.Scanner;


public class lab3_2
{
	public static void generuj (int tab[], int n)
	{
		for(int i = 0; i < n; i++)
		{
			tab[i] = (int)(Math.random()*2000)-1000;
			System.out.print(tab[i] + " ");
		}
	}



	public static int ileNieparzystych (int tab[])
	{
		int p = 0;
		for(int i = 0; i < tab.length; i++)
		{
			if(tab[i]%2 != 0) p++;
		}
		return p;
	}


	public static int ileParzystych (int tab[])
	{
		int p = 0;
		for(int i = 0; i < tab.length; i++)
		{
			if(tab[i]%2 == 0) p++;
		}
		return p;
	}

	public static int ileDodatnich (int tab[])
	{
		int dod=0;
		for(int i=0; i<tab.length; i++)
		{
		if(tab[i]>0) dod++;
		}
		return dod;
	}

	public static int ileUjemnych (int tab[])
	{
		int uj=0;
		for(int i=0; i<tab.length; i++)
		{
		if(tab[i]<0) uj++;
		}
		return uj;
	}

	public static int ileZerowych (int tab[])
	{
		int zer=0;
		for(int i=0; i<tab.length; i++)
		{
		if(tab[i]==0) zer++;
		}
		return zer;
	}

	public static int ileMaksymalnych (int tab[])
	{
		int max=tab[0];
		int cnt=1;
		for(int i = 1; i<tab.length; i++)
		{
			if(tab[i]==max) cnt++;
			else if(tab[i]>max) cnt=1;
		}
		return cnt;
	}

	public static int sumaDodatnich (int tab[])
	{
		int dod=0;
		for(int i=0; i<tab.length; i++)
		{
		if(tab[i]>0) dod+=tab[i];
		}
		return dod;
		
	}

	public static int sumaUjemnych (int tab[])
	{
		int uj=0;
		for(int i=0; i<tab.length; i++)
		{
		if(tab[i]<0) uj+=tab[i];
		}
		return uj;		
	}

	public static int dlugoscMaksymalnegoCiaguDodatnich (int tab[])
	{
		int najdltmp=0;
		int najdl=0;
		for(int i = 0; i < tab.length; i++)
		{
			if(tab[i]>0) najdltmp++;
			else
			{
				if(najdltmp>najdl) najdl=najdltmp;
				najdltmp=0;
			}
		}
		return najdl;
	}

	public static void signum(int tab[])
	{
		for(int i = 0; i<tab.length; i++)
		{
			if(tab[i]>0) tab[i]=1;
			if(tab[i]<0) tab[i]=-1;
		}
	}

	public static void odwrocFragment(int tab[], int lewy, int prawy)
	{
		for(int i = prawy; i>=lewy; i--)
		{
			System.out.print(tab[i]+" ");
		}
		
	}



	public static void main(String[] args) 
	{
		Scanner in = new Scanner(System.in);
		int n = -1;
		while(n<1 || n>100)
		{
		System.out.print("n=?");
		n = in.nextInt();
		if(n<1 || n>100) System.out.println("Wczytaj ponownie");
		int[] arr = new int[n];
		generuj(arr,n);
		System.out.println("Nieparzystych");
		System.out.println(ileNieparzystych(arr));
		System.out.println("Parzystych");
		System.out.println(ileParzystych(arr));
		System.out.println("Dodatnich");
		System.out.println(ileDodatnich(arr));
		System.out.println("Ujemnych");
		System.out.println(ileUjemnych(arr));
		System.out.println("Zer");
		System.out.println(ileZerowych(arr));
		System.out.println("Maksymalnych");
		System.out.println(ileMaksymalnych(arr));
		System.out.println("sumaDodatnich");
		System.out.println(sumaDodatnich(arr));
		System.out.println("sumaUjemnych");
		System.out.println(sumaUjemnych(arr));
		System.out.println("dlugoscMaksymalnegoCiaguDodatnich");
		System.out.println(dlugoscMaksymalnegoCiaguDodatnich(arr));
		signum(arr);
		for(int i = 0; i<arr.length; i++)
		{
				System.out.print(arr[i] + " ");
		}
		int lewy=-1;
		int prawy=-1;
		while(lewy<1 || lewy>n || prawy<1 || prawy>n)
		{
			lewy = in.nextInt();
			prawy = in.nextInt();
			if(lewy<1 || lewy>n || prawy<1 || prawy>n) System.out.println("Ponownie!");
		}
		System.out.println("Odwrocone");
		odwrocFragment(arr,lewy,prawy);
		}
	}
}