import java.util.*;
import java.text.SimpleDateFormat;

public class Lab11_3
{
    public static void main(String[] args)
    {
        Integer[] a = {5,5,6,2,1,23,4,6,7,8};
        ArrayUtil.sort(a,0,a.length-1);
        for(int i =0 ; i<a.length; i++)
        System.out.println(a[i]);
    }
}

class ArrayUtil
{
    public static <T extends Comparable<T>> boolean isSorted(T[] a)
    {
        boolean is = true;
        for(int i = 0; i<a.length-2; i++)
        {
            if(a[i].compareTo(a[i+1])==1) is = false;
        }
        return is;
    }
    public static <T extends Comparable<T>> int binSearch(T[] a, T Sz)
    {
        int index = -1;
        int low = 0;
        int high = a.length - 1;
        while(low<=high)
        {
            int mid = (low + high)/2;
            if(a[mid].compareTo(Sz)==-1) low = mid+1;
            else if(a[mid].compareTo(Sz)==1) high = mid-1;
            else if(a[mid].compareTo(Sz)==0)
            {
                index = mid;
                break;
            }
        }
        return index;
    }
    public static <T extends Comparable<T>> void selectionSort(T[] arr)
    {
        int n = arr.length;
        for(int i = 0; i<n-1; i++)
        {
            int min_idx = i;
            for(int j = i+1; j<n; j++)
                if(arr[j].compareTo(arr[min_idx])==-1)
                    min_idx = j;
            T temp = arr[min_idx];
            arr[min_idx] = arr[i];
            arr[i] = temp;
        }
    }
    public static <T extends Comparable<T>>  void merge(T arr[], int l, int m, int r) 
    { 
        int n1 = m - l + 1; 
        int n2 = r - m; 
        ArrayList<T> L = new ArrayList<T>();
        //T L[] = new T [n1]; 
        ArrayList<T> R = new ArrayList<T>();
        //T R[] = new T [n2]; 
        for (int i=0; i<n1; ++i) 
            L.add(arr[l + i]); 
        for (int j=0; j<n2; ++j) 
            R.add(arr[m + 1+ j]); 
        int i = 0, j = 0; 
        int k = l; 
        while (i < n1 && j < n2) 
        { 
            if (L.get(i).compareTo(R.get(j))!=-1) 
            { 
                arr[k] = L.get(i); 
                i++; 
            } 
            else
            { 
                arr[k] = R.get(j); 
                j++; 
            } 
            k++; 
        } 
        while (i < n1) 
        { 
            arr[k] = L.get(i); 
            i++; 
            k++; 
        } 
        while (j < n2) 
        { 
            arr[k] = R.get(j); 
            j++; 
            k++; 
        } 
    } 
    public static <T extends Comparable<T>> void sort(T arr[], int l, int r) 
    { 
        if (l < r) 
        { 
            int m = (l+r)/2; 
            sort(arr, l, m); 
            sort(arr , m+1, r); 
            merge(arr, l, m, r); 
        } 
    }
}



