import java.util.Scanner;
import java.util.Collections;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
public class reader
{
    public static void main(String[] args)
    {
        if (args.length != 1) {
            System.err.println("Sposób użycia: java TestFileScanner nazwaPliku");
            System.exit(1);
        }
        Scanner file;
        ArrayList<String> Strings = new ArrayList<String>();
        try {
            file = new Scanner(new File(args[0]));
            while (file.hasNextLine()) {
                String line = file.nextLine();
                Strings.add(line);
            }
        } catch (FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
            System.exit(1);
        } catch (Exception e) {
            System.err.println("Caught Exception: " + e.getMessage());
            System.exit(2);
        } finally {
            Collections.sort(Strings);
            for(String p : Strings) System.out.println(p);
        }
        
    }
}

