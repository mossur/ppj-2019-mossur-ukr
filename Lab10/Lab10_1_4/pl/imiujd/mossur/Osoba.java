package pl.imiujd.mossur;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.Period;

public class Osoba implements Comparable<Osoba>, Cloneable
{
    public Osoba(String nazwisko, LocalDate dataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
        
    }
    public String toString()
    {
        return getClass().getName()
               + "[nazwisko=" + nazwisko
               + ",dataUrodzenia=" + dataUrodzenia.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"))
               + "]";
    }
    public boolean equals(Object otherObject)
    {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        Osoba other = (Osoba) otherObject;
        return nazwisko.equals(other.nazwisko)
               && dataUrodzenia.equals(other.dataUrodzenia);
    }
    public int compareTo(Osoba other)
    {
        int result = nazwisko.compareTo(other.nazwisko);
        if(result!=0) return result;
        result = dataUrodzenia.compareTo(other.dataUrodzenia);
        return result;
    }
    public int ileLat()
    {
         Period prd = Period.between(this.dataUrodzenia, LocalDate.now());
         return prd.getYears();
    }
    public int ileMiesiecy()
    {
         Period prd = Period.between(this.dataUrodzenia, LocalDate.now());
         return prd.getMonths();
    }
    public int ileDni()
    {
         Period prd = Period.between(this.dataUrodzenia, LocalDate.now());
         return prd.getDays();
    }
    private String nazwisko;
    private LocalDate dataUrodzenia;
}
