import pl.imiujd.mossur.*;
import java.util.*;
import java.time.LocalDate;
import java.util.ArrayList;
public class TestStudent
{
    public static void main(String[] args)
    {
        ArrayList<Student> grupa = new ArrayList<Student>();
        grupa.add(new Student("Tetiana", LocalDate.of(2001, 12, 06), 5.0));
        grupa.add(new Student("Nadia", LocalDate.of(1999, 11, 26), 4.0));
        grupa.add(new Student("Diana", LocalDate.of(2000, 05, 26), 3.0));
        grupa.add(new Student("Sasha", LocalDate.of(1999, 11, 26), 5.0));
        grupa.add(new Student("Denis", LocalDate.of(2000, 01, 06), 3.0));
        for (Student p : grupa) {
            System.out.println(p);
        }
        Collections.sort(grupa);
        System.out.println("After sort");
        for (Student p : grupa) {
            System.out.println(p);
        }
    }
}

